import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
#%matplotlib inline

def importTowerData(filename):
    Towers=pd.read_csv(filename)   #,encoding="ISO-8859-1"
    #Towers.head()
    N=len(Towers)
    Pos=np.zeros((2,N))
    Pos[0,:]=Towers['latitude']
    Pos[1,:]=Towers['longitude']
    return Pos