#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np
import math
import networkx as nx
import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')

def euclidean_distance(row1, row2):
    distance = 0.0
    for i in range(len(row1)):
        distance += (row1[i] - row2[i])**2
    return math.sqrt(distance)

# Locate the most similar neighbors
def get_neighbors(train, test_row, num_neighbors):
    distances = list()
    for train_row in train:
        dist = euclidean_distance(test_row, train_row)
        distances.append((train_row, dist))
    distances.sort(key=lambda tup: tup[1])
    neighbors = list()
    for i in range(num_neighbors):
        neighbors.append(distances[i][0])
    return neighbors

import math
def distance(p1,p2):
    '''Returns the distance betwen two numpy points'''
    return math.sqrt((p1[0] - p2[0])**2 + (p1[1] - p2[1])**2)
    
def kNN(points, K = 2):
    '''Generates an adjacency matrix based on the Nearest neighbor graph algorithm.'''
    points = list(map(list, zip(*points))) #magic command to transpose a list of lists
    G = np.zeros((len(points), len(points)))
    for p in range(0,len(points)):
        distances = []
        for n in range(0,len(points)):
            d = euclidean_distance(points[p], points[n])
            distances.append(d)
        index = np.argsort(distances)
        for i in range(0,K+1):
            G[p,index[i]] = 1
    return G
# # In[2]:


# neighbors = get_neighbors(latlong, latlong[0],50)


# # In[3]:


# neighbors=nx.Graph(neighbors)
# options = {'node_color': 'red','node_size': 50,'line_color': 'grey','linewidths': 0.1,'width': 0.5,}
# nx.draw(neighbors, **options)
# plt.axis('on')
# plt.xlabel("latitude")
# plt.ylabel("longitude")
# plt.grid()
# plt.show()


# # In[ ]:




