#!/usr/bin/env python
# coding: utf-8

# In[ ]:
import networkx as nx
import numpy as np
def GreedyColorGraph(D,M):
    G = nx.Graph(D)
    colors = nx.greedy_color(G, strategy = "largest_first")
    color_list = []
    for i in range(len(colors)):
        color_list.append(colors[i])
    colors = color_list
    return colors

