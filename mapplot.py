import folium
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import to_hex

def mapplot(locations, colors, cmap=plt.cm.jet):
	M = max(colors)
	locations = np.array(locations).T
	colors = np.array(colors)
	# inputs check
	if len(locations) != len(colors):
		print('> ERROR - colors length does not match locations length')
		return
	else:
		nloc = len(locations)
	# convert index of colors into RGBA codes
	rgba_codes = cmap(colors / (M))
		
	# set up the folium map object centered at centroid of all locations
	centroid = locations.mean(axis=0)
	fmap = folium.Map(location=centroid)
	
	# specify the range of locations to determine a suitable zoom level
	location_southwest = [locations[:,0].min(), locations[:,1].min()]
	location_northeast = [locations[:,0].max(), locations[:,1].max()]
	fmap.fit_bounds([location_southwest, location_northeast])
	
	# add markers to fmap
	for i in range(nloc):
		html_code = to_hex(rgba_codes[i])
		marker_icon = folium.Icon(icon='cloud', color='white', icon_color=html_code)
		marker = folium.Marker(location=locations[i], icon=marker_icon)
		marker.add_to(fmap)
	
	return fmap
